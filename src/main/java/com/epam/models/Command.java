package com.epam.models;

@FunctionalInterface
public interface Command {

     String startCommand(String argument);
}

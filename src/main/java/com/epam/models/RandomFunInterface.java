package com.epam.models;

import java.util.List;

@FunctionalInterface
public interface RandomFunInterface {

    List<Integer> random();
}

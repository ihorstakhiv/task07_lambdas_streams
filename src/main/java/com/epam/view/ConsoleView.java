package com.epam.view;

import com.epam.controllers.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView implements AbstractView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private static Logger logger = LogManager.getLogger(Controller.class);

    public ConsoleView() {
        controller = new Controller();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\n  1 - Put three digits and you can see max digit");
        menu.put("2", "\n  2 - Put three digits and you can see average");
        menu.put("3", "\n  3 - Put word and get it with UPPER letters");
        menu.put("4", "\n  4 - Put word and get it with low letters");
        menu.put("5", "\n  5 - Put word and get is Empty or not");
        menu.put("6", "\n  6 - Show three randoms Lists");
        menu.put("7", "\n  7 - Show average,min,max,sum of list value");
        menu.put("8", "\n  8 - Count number of values that are bigger than average");
        menu.put("9", "\n  9 - Create words list, you get:" +
                "\n-number of unique words" +
                "\n-unique words list" +
                "\n-word count" +
                "\n");
        menu.put("Q", "\n  Q - exit\n");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printThreeDigitsMax);
        methodsMenu.put("2", this::printThreeDigitsAverage);
        methodsMenu.put("3", this::printWordWithUpperLetter);
        methodsMenu.put("4", this::printWordWithLowLetter);
        methodsMenu.put("5", this::printIsEmptyOrNot);
        methodsMenu.put("6", this::printThreeRandomsLists);
        methodsMenu.put("7", this::printListParameters);
        methodsMenu.put("8", this::printCountValuesThatAreBiggerThanAverage);
        methodsMenu.put("9", this::printApplication);
    }


    private int putDigit() {
        logger.info("Put your digit:");
        return input.nextInt();
    }

    private String putWord() {
        logger.info("Put your word:");
        return input.next();
    }

    private void printThreeDigitsMax() {
        logger.info("Please enter 3 digits");
        logger.info(controller.maxValue.getThreeInts(putDigit(), putDigit(), putDigit()));
    }

    private void printThreeDigitsAverage() {
        logger.info("Please enter 3 digits");
        logger.info(controller.average.getThreeInts(putDigit(), putDigit(), putDigit()));
    }

    private void printWordWithUpperLetter() {
        logger.info(controller.commandToUpperCase.startCommand(putWord()));
    }

    private void printWordWithLowLetter() {
        logger.info(controller.commandToLowerCase.startCommand(putWord()));
    }

    private void printIsEmptyOrNot() {
        logger.info(controller.isEmpty(putWord()));
    }

    private void printThreeRandomsLists() {
        logger.info("First list:");
        logger.info(controller.firstRandomList.random());
        logger.info("Second list:");
        logger.info(controller.secondRandomList.random());
        logger.info("Third list:");
        logger.info(controller.thirdRandomList.random());
    }

    private void printListParameters() {
        logger.info("Average:");
        logger.info(controller.countAverage(controller.firstRandomList.random()));
        logger.info("Min:");
        logger.info(controller.listMinValue(controller.firstRandomList.random()));
        logger.info("Max:");
        logger.info(controller.listMaxValue(controller.firstRandomList.random()));
        logger.info("Sum of list value:");
        logger.info(controller.sumListValues(controller.firstRandomList.random()));
    }

    private void printCountValuesThatAreBiggerThanAverage() {
        logger.info(controller.countValueMoreThenAvarage(controller.firstRandomList.random()));
    }

    private void printApplication() {
        logger.info("Put your words, if you set space the application will be start: ");
        String input = this.input.next();
        if (input.equals(" ")) {
            logger.info("Words count :" + controller.worldCount(controller.ownList()));
            logger.info("Number of unique words: " + controller.numberUniqueWords(controller.ownList()));
            logger.info("Sorted list :" + controller.sortedUniqueWords(controller.ownList()));
        } else
            controller.ownList().add(input);
        printApplication();
    }

    private void outputMenu() {
        logger.info("\n**************       MENU       ***************\n");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}


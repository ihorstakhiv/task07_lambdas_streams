package com.epam.controllers;

import com.epam.models.Command;
import com.epam.models.CommandObject;
import com.epam.models.RandomFunInterface;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Controller {

    public FunctionalInterfaceGetThreeInts maxValue = (first, second, third) -> {
        List<Integer> fullList = new ArrayList<>();
        fullList.add(first);
        fullList.add(second);
        fullList.add(third);
        List<Integer> sortedList = fullList
                .stream()
                .sorted((o1, o2) -> o2 - o1)
                .collect(Collectors.toList());
        return sortedList.get(0);
    };

    public FunctionalInterfaceGetThreeInts average = (first, second, third) -> {
        return (first + second + third) / 3;
    };

    public Command commandToUpperCase = String::toUpperCase;

    public Command commandToLowerCase = new Command() {
        @Override
        public String startCommand(String argument) {
            return argument.toLowerCase();
        }
    };

    public String isEmpty(String argument) {
        return new CommandObject().startCommand(argument);
    }

    public RandomFunInterface firstRandomList = () -> {
        List<Integer> list = new ArrayList<>();
        randomGenerator(list);
        return list;
    };

    public RandomFunInterface secondRandomList = () -> {
        List<Integer> list = new ArrayList<>();
        randomGenerator(list);
        return list;
    };

    public RandomFunInterface thirdRandomList = () -> {
        List<Integer> list = new ArrayList<>();
        randomGenerator(list);
        System.out.println(list);
        return list;
    };

    private void randomGenerator(List<Integer> list) {
        Random random = new Random();
        for (int i = 0; i < 7; i++) {
            list.add(random.nextInt(50));
        }
    }

    public int countValueMoreThenAvarage(List<Integer> list) {
        return list.stream()
                   .filter(integer -> integer.doubleValue() > countAverage(list))
                   .collect(Collectors.toList())
                   .size();
    }

    public double countAverage(List<Integer> list) {
        return list.stream()
                   .collect(Collectors.summarizingInt(value -> value))
                   .getAverage();
    }

    public int listMaxValue(List<Integer> list) {
        return list.stream()
                   .collect(Collectors.summarizingInt(value -> value))
                   .getMax();
    }

    public int listMinValue(List<Integer> list) {
        return list.stream()
                   .collect(Collectors.summarizingInt(value -> value))
                   .getMin();
    }

    public long sumListValues(List<Integer> list) {
        return list.stream()
                   .collect(Collectors.summarizingInt(value -> value))
                   .getSum();
    }

    public List<String> ownList(){
        return  new LinkedList<>();
    }

    public long numberUniqueWords(List<String> list){
    return list.stream()
               .distinct()
               .count();
    }

    public List<String> sortedUniqueWords(List<String> list){
        return list.stream()
                   .distinct()
                   .sorted((o1, o2) -> o1.compareTo(o2))
                   .collect(Collectors.toList());
    }

    public long worldCount(List<String> list){
        return list.size();
    }
}

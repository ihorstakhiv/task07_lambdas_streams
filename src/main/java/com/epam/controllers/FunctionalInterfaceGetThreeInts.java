package com.epam.controllers;

@FunctionalInterface
public interface FunctionalInterfaceGetThreeInts {

   int getThreeInts(int first,int second,int third);
}
